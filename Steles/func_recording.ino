

// Toggle into, or out of, EDIT MODE
void toggleEditMode() {
  
  // Note: This call to concludeHeldNote() also empties out the HELDBUTTON flag (HELDBUTTON|128)
  concludeHeldNote(0, 1); // Conclude held note, not recording it, but sending a NOTE-OFF
  
  TO_WRITE = 0; // Clear the "a command needs to be recorded after the tick is over" flag, if applicable
  
  HCHAN[0] = 255; // Set HOCKET to "inactive"
  SCOUNT = 0; // Clear any residual SHIFT-countdown
  
  resetTiming(); // Reset timing of all seqs and the global-cue, and send a SONG-POSITION POINTER if CLOCK SOURCE is internal
  
  writePrefs(); // Write the current relevant global vars into the prefs-file
  
  if (EDITMODE) { // If EDITMODE is about to be untoggled...
    
    updateSeqHeader(); // Update the header-data-bytes for the current seq
    writeTempo(); // Update the savefile's header-bytes for TEMPO if they don't match current values
    writeTimeSignature(); // Update the savefile's header-bytes for TIMESIG1 and TIMESIG2 if they don't match current values
    
  } else { // Else, if EDIT MODE is about to be toggled...
    
    updateNonMatch(FILE_RANGE_BYTE, RANGE); // Update the savefile's header-byte for RANGE
    
    memset(CMD, 0, 48); // Clear all sequences' CUE-commands (this prevents weird behavior from occurring when EDIT MODE is toggled with CUEs active)
    
    if (!(STATS[RECORDSEQ] & 128)) { // If the most-recently-touched sequence isn't PLAYING...
      
      if (ACTIVE_SEQS == MAX_ACTIVE_SEQS) { // If the maximum number of seqs are already playing...
        for (byte i = 0; i < 48; i++) { // For every sequence...
          if (STATS[i] & 128) { // If this seq is playing...
            STATS[i] &= 127; // Empty out its PLAYING-bit
            break; // Break the for-loop, since only one of the active seqs should be turned off here
          }
        }
      } else { // Else, if the maximum number of seqs aren't playing...
        ACTIVE_SEQS++; // Increase the var that tracks how many seqs are playing
      }
      
      STATS[RECORDSEQ] |= 128; // Flag the most-recently-touched sequence to be PLAYING, if it isn't already
      
    }
    
  }
  
  RECORDNOTES = 0; // Disarm note-recording, if applicable
  EDITMODE ^= 1; // Toggle or untoggle EDIT MODE
  
  clearBlink(); // Clear any current BLINKs/GLYPHs
  
  TO_UPDATE = 255; // Flag all LED-rows for updating
  
}

// Write a given 5-byte command into a given command-slot in the savefile,
// only committing actual writes when the data doesn't match.
void writeCommand(
  unsigned long pos // Note's bitwise position in the savefile
) {
  
  file.seekSet(pos); // Navigate to the data's position
  file.read(BUFREAD, 5); // Read the already-existing data from the savefile into a data-buffer
  
  for (byte i = 0; i <= 4; i++) { // For every byte in the given 5-byte command...
    if (
      ((!MODIFY) || (MODIFY & (16 >> i))) // If MODIFY is inactive, or an active MODIFY-bit corresponds to this byte-slot...
      && (BUFREAD[i] != BUFWRITE[i]) // And the byte in the seq doesn't already match the to-be-written byte...
    ) {
      file.seekSet(pos + i); // Navigate to the byte's position in the savefile
      file.write(BUFWRITE[i]); // Write the to-be-written byte into that position
    }
  }
  
  file.sync(); // Immediately synchronize the updated savefile-data, to reduce risk of data-corruption
  
}

// Erase a tick's worth of notes, in the current TRACK, in the current RECORDSEQ, at the given position.
void eraseTick(word pos) {
  memset(BUFWRITE, 0, 5); // Clear the command-write buffer, to signify a blank note
  writeCommand(FILE_BODY_START + (BYTES_PER_SEQ * RECORDSEQ) + (pos * 10) + (TRACK * 5)); // Write the blank note to its corresponding savefile position
}

// Check whether both STEP-SEQUENCING is enabled and RECORDNOTES is armed, and if both are true, move the step-sequencing system forward
void stepSeqForward() {
  if (RECORDNOTES && (CLOCKSOURCE == 255)) { // If RECORD NOTES is armed, and STEP-SEQUENCING is enabled...
    for (byte i = 0; i < QUANTIZE; i++) { // For every tick between the current tick-position and the next tick-position...
      processSustains(); // Instantly process one tick's worth of duration for all notes in the SUSTAIN system
    }
    shiftCuePoint(QUANTIZE); // Shift the GLOBAL CUE by the QUANTIZE amount, wrapping it around its TIMESIG-based limits
    shiftPlayingSeqs(QUANTIZE); // Shift every playing-seq's position by the QUANTIZE amount, wrapping them around their SIZE-based limits
  }
}

// Parse an incoming AUTO-DUR MIDI-command from either the grid-buttons or the MIDI-IN port (this function is only called from Edit Mode).
void parseAutoInput(
  byte fromwhere, // Flag for: Is this command coming from the grid-buttons (0) or the MIDI-IN port (1)?
  byte button, // The command's corresponding grid-button. Will always be set to "127" for MIDI-IN notes.
  byte b0, // First command-byte (high nibble = command type; low nibble = MIDI-CHANNEL)
  byte b1, // First payload-byte
  byte b2 // Second payload-byte, or 0 for a 2-byte command
) {
  
  TO_WRITE = 0; // Empty out TO_WRITE, to prevent conflicts between simultaneous grid-button-commands and MIDI-IN-port commands
  
  if (REPEAT && (!fromwhere)) { // If REPEAT is active, and this input is from the button-grid...
    
    arpPress(); // Parse a new keypress in the arpeggiation-system
    
  } else { // Else, if REPEAT is inactive *or* this input is from the MIDI-IN port...
    
    if ((b0 & 240) == 128) { // If the incoming command is a NOTE-OFF...
      byte dorec = // Get the following truth-value:
        (BUFWRITE[1] == b1) // "The held-pitch is the same as the incoming-pitch..."
        && ((HELDBUTTON & 127) == button); // "And the held-button is the same as the incoming-button (always 127 for MIDI-IN-port cmds)"
      concludeHeldNote(dorec, 1); // Conclude held note, recording it and sending a NOTE-OFF
      return; // Exit the function, as no new note has been received
    }
    
    HELDBUTTON = 128 | button; // Set the held-button to signify both "currently held" (|128) and the current button-position
    HELDPOS = applyOffset(1, applyQuantize(POS[RECORDSEQ])); // Track the position in RECORDSEQ where this note started, with offset/quantize
    BUFWRITE[0] = b0; // First held byte: Command-type and MIDI-CHANNEL
    BUFWRITE[1] = b1; // Second held byte: Pitch (or corresponding data-payload-byte)
    BUFWRITE[2] = modVelo(b2); // Third held byte: Velocity (or corresponding data-payload-byte), with HUMANIZE applied
    BUFWRITE[3] = DURATION; // Fourth held byte: Default auto-DURATION value
    BUFWRITE[4] = CHANCE; // Fifth held byte: The current user-entered CHANCE value
    
    validateHeldBytes(); // Ensure that the command's payload-bytes are in the correct format for their command-type
    
    playAutoNote(); // Play an AUTO-DURATION note
    
    if (fromwhere) { // If this came from the MIDI-IN port...
      
      byte cmd = BUFWRITE[0] & 240; // Get the new held-command's command-type, stripped of CHANNEL information
      
      if (cmd == 144) { // If this is a NOTE-ON...
        
        concludeHeldNote(1, 0); // Conclude held note, recording it, but not sending any NOTE-OFF
        stepSeqForward(); // Move the step-sequencing position forward, if STEP-SEQUENCING is currently enabled
        
      } else if (CLOCKSOURCE != 255) { // Else, if this is *not* a NOTE-ON, and STEP-SEQUENCING *isn't* active...
        
        TO_WRITE = 1; // Flag the held-command to be written later
        
      } else { // Else, if this isn't NOTE-ON, and STEP-SEQUENCING is enabled, the held-note should just be discarded:
        
        HELDBUTTON &= 127; // Clear the "held note" flag (this flag is stored in HELDBUTTON|128)
        
      }
      
    } else { // Else, if this came from a grid-button press...
      
      concludeHeldNote(1, 0); // Conclude held note, recording it, but not sending any NOTE-OFF
      stepSeqForward(); // Move the step-sequencing position forward, if STEP-SEQUENCING is currently enabled
      
    }
    
  }
  
}

// Parse an incoming MANUAL-DUR MIDI-command from either the grid-buttons or the MIDI-IN port (this function is only called from Edit Mode).
void parseManualInput(
  byte fromwhere, // Flag for: Is this command coming from the grid-buttons (0) or the MIDI-IN port (1)?
  byte button, // The command's corresponding grid-button. Will always be set to "127" for MIDI-IN notes.
  byte b0, // First command-byte (high nibble = command type; low nibble = MIDI-CHANNEL)
  byte b1, // First payload-byte
  byte b2 // Second payload-byte, or 0 for a 2-byte command
) {
  
  byte special = (b0 >= 160) || (b0 <= 127); // Figure out if this is a non-NOTE-ON, non-NOTE-OFF command
  
  if (
    special // If the incoming command is a non-NOTE-ON, non-NOTE-OFF command...
    && (HELDBUTTON & 128) // And a command is currently held...
    && ((BUFWRITE[0] & 240) == 144) // And the held-command is a NOTE-ON...
  ) {
    concludeHeldNote(1, 1); // Conclude held note, recording it and sending a NOTE-OFF
  }
  
  if (
    special // If the incoming command is a non-NOTE-ON, non-NOTE-OFF command...
    || (RECORDNOTES && (CLOCKSOURCE == 255)) // Or, RECORD NOTES is armed, and STEP-SEQUENCING is enabled...
  ) {
    parseAutoInput(fromwhere, button, b0, b1, b2); // Parse an AUTO-DUR command instead of a MANUAL-DUR command
    return; // Exit this function, having performed its equivalent AUTO-DURATION function instead
  }
  
  byte cmd = b0 & 240; // Get the command-type of the given MIDI-command, stripped of its MIDI-CHANNEL information
  
  TO_WRITE = 0; // Empty out TO_WRITE, to prevent conflicts between simultaneous grid-button-commands and MIDI-IN-port commands
  
  if (
    ((BUFWRITE[0] & 240) == 144) // If the held-command is a NOTE-ON...
    && ( // And also:
      (cmd == 144) // The incoming command is either a NOTE-ON...
      || ( // Or...
        (cmd == 128) // The incoming command is a NOTE-OFF...
        && (BUFWRITE[1] == b1) // And the held-pitch is the same as the incoming-pitch...
        && ((HELDBUTTON & 127) == button) // And the held-button is the same as the incoming-button (always 127 for MIDI-IN-port cmds)...
      )
    )
  ) {
    concludeHeldNote(1, 1); // Conclude held note, recording it and sending a NOTE-OFF
  }
  
  if (cmd == 128) { return; } // If the incoming command was a NOTE-OFF, exit the function so it doesn't overwrite any held-command
  
  HELDBUTTON = 128 | button; // Set the held-button to signify both "currently held" (|128) and the current button-position
  HELDPOS = applyOffset(1, applyQuantize(POS[RECORDSEQ])); // Track the position in RECORDSEQ where this note started, with offset/quantize
  BUFWRITE[0] = b0; // First held byte: Command-type and MIDI-CHANNEL
  BUFWRITE[1] = b1; // Second held byte: Pitch (or corresponding data-payload-byte)
  BUFWRITE[2] = modVelo(b2); // Third held byte: Velocity (or corresponding data-payload-byte), with HUMANIZE applied
  BUFWRITE[3] = 0; // Fourth held byte: 0, which will count upwards while MANUAL-DURATION is held
  BUFWRITE[4] = CHANCE; // Hold the current user-entered CHANCE value
  
  validateHeldBytes(); // Ensure that the command's payload-bytes are in the correct format for their command-type
  
  if (BUFWRITE[0] & 128) { // If the new held-command isn't a Tine-specific internal command (i.e. it has a value >=128)...
    playNote(); // Play the held-command, or the beginning of the held-note if it's a NOTE-ON
  }
  
}


