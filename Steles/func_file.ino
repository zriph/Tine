

// Check whether savefiles exist, and create whichever ones don't exist
void createFiles() {
  
  char name[8]; // Will hold a given filename
  
  for (byte i = 0; i < TOTAL_SONG_FILES; i++) { // For every song-slot...
    
    sendMaxCmd(1, i); // Send the top LED-row a binary value showing which file is about to be created
    
    getFilename(name, i); // Get the filename that corresponds to this song-slot
    
    if (sd.exists(name)) { continue; } // If the file exists, skip the file-creation process for this filename
    
    file.createContiguous(name, FILE_BYTES); // Create a contiguous file
    file.close(); // Close the newly-created file
    
    file.open(name, O_WRITE); // Open the file explicitly in WRITE mode
    
    writeTempo(); // Update the savefile's header-bytes for TEMPO if they don't match current values
    writeTimeSignature(); // Update the savefile's headder-bytes for TIMESIG1 and TIMESIG2 if they don't match current values
    
    updateNonMatch(FILE_RANGE_BYTE, RANGE); // Update the savefile's header-byte for RANGE
    
    for (byte j = FILE_SQS_START; j <= FILE_SQS_END; j++) { // For every seq-size byte in the header...
      file.seekSet(j); // Go to that byte's position
      file.write(byte(DEFAULT_SEQ_SIZE)); // Write the default sequence-size value
    }
    
    file.close(); // Close the file
    
  }
  
}

// Get the name of a target song-slot's savefile (format: "000.DAT" etc.)
void getFilename(char source[], byte fnum) {
  //source[0] = (char(fnum / 100) % 10) + 48; // NOTE: this line is only needed if savefiles reach "100"
  source[0] = 48;
  source[1] = (char(fnum / 10) % 10) + 48;
  source[2] = char((fnum % 10) + 48);
  source[3] = 46;
  source[4] = 68;
  source[5] = 65;
  source[6] = 84;
  source[7] = 0;
}

// Update a given byte within a given song-file.
// Note: This is used only to update various bytes in the header, so "byte pos" is acceptable.
void updateFileByte(byte pos, byte b) {
  file.seekSet(pos); // Go to the given insert-point
  file.write(b); // Write the new byte into the file
  file.sync(); // Make sure the changes are recorded
}

// Check whether a given header-byte is a duplicate, and if not, update it in the savefile.
// Note: This is used only to update various bytes in the header, so "byte pos" is acceptable.
void updateNonMatch(byte pos, byte b) {
  file.seekSet(pos); // Go to the corresponding byte in the savefile
  if (file.read() != b) { // Read the header-byte. If it isn't the same as the given byte-value...
    updateFileByte(pos, b); // Put the local header-byte into its corresponding savefile header-byte
  }
}

// Update the header-data-bytes for the current seq:
// Both SEQ SIZE (bits 0-5) and ACTIVE ON LOAD (bit 6). (bit 7 is intentionally stripped)
void updateSeqHeader() {
  updateNonMatch(FILE_SQS_START + RECORDSEQ, STATS[RECORDSEQ] & 127); // Update the seq's saved SIZE and ACTIVE ON LOAD bits
}

// Update the savefile's header-bytes for TEMPO if they don't match current values
void writeTempo() {
  
  // Check both bytes of TEMPO in the savefile's header, and update them if they've been changed.
  updateNonMatch(FILE_TEMPO_BYTE1, byte(TEMPO >> 8));
  updateNonMatch(FILE_TEMPO_BYTE2, byte(TEMPO & 255));
  
}

// Update the savefile's header-bytes for TIMESIG1 and TIMESIG2 if they don't match current values
void writeTimeSignature() {
  updateNonMatch(FILE_TIMESIG1_BYTE, TIMESIG1);
  updateNonMatch(FILE_TIMESIG2_BYTE, TIMESIG2);
}

// Load the TEMPO information from the current open file,
// and compensate for files where any such values have been edited to fall outside the valid ranges.
void getTempo() {
  
  file.seekSet(FILE_TEMPO_BYTE1); // Go to the first TEMPO-byte location
  TEMPO = word(file.read()) << 8; // Read it into the highest 8 bits of a word
  file.seekSet(FILE_TEMPO_BYTE2); // Go to the second TEMPO-byte location
  TEMPO |= word(file.read()); // Read it and combine it with the first byte
  
  // If an erroneous value falls outside of the valid TEMPO range...
  if (TEMPO < TEMPO_LIMIT_LOW) {
  //if ((TEMPO < TEMPO_LIMIT_LOW) || (TEMPO > TEMPO_LIMIT_HIGH)) {////////////////////////
    TEMPO = DEFAULT_TEMPO; // Set the TEMPO to the default value
  }
  
}

// Get a RANGE value from the savefile's header-bytes
void getRange() {
  file.seekSet(FILE_RANGE_BYTE); // Go to the RANGE-byte location
  RANGE = file.read(); // Read the RANGE value
  CURTICK %= word(RANGE) * word(TIMESIG3); // Wrap the current tempo-tick around to fit the new RANGE value, if applicable
}

// Get the song's default TIMESIG1 and TIMESIG2 values, and then use them to compute a TIMESIG3 value
void getTimeSignature() {
  file.seekSet(FILE_TIMESIG1_BYTE); // Go to the TIMESIG-dividend-byte location
  TIMESIG1 = file.read(); // Read the TIMESIG1 value
  file.seekSet(FILE_TIMESIG2_BYTE); // Go to the TIMESIG-divisor-byte location
  TIMESIG2 = file.read(); // Read the TIMESIG2 value
  TIMESIG3 = TIMESIG1 * TIMESIG2; // Calculate TIMESIG3 from the newly-loaded values (TIMESIG3 is not stored in savefiles)
}

// Put the current prefs-related global vars into a given buffer
void makePrefBuf(byte buf[]) {
  buf[0] = MODIFY;
  buf[1] = OCTAVE;
  buf[2] = VELO;
  buf[3] = HUMANIZE;
  buf[4] = CHAN;
  buf[5] = QRESET;
  buf[6] = QUANTIZE;
  buf[7] = DURATION;
  buf[8] = REPEAT;
  buf[9] = OFFSET;
  buf[10] = SONG;
  buf[11] = CLOCKSOURCE;
  buf[12] = GRIDCONFIG;
  buf[13] = CHANCE;
  buf[14] = 0; // reserved
  buf[15] = 0; // reserved
  buf[16] = 0; // reserved
}

// Write the current relevant global vars into the prefs-file
void writePrefs() {
  
  file.close(); // Temporarily close the current song-file
  
  byte buf[PREFS_ITEMS_2]; // Make a buffer that will contain all the prefs
  makePrefBuf(buf); // Fill it with all the relevant pref values
  
  byte cbuf[PREFS_ITEMS_2]; // Make a buffer for checking the old prefs-contents
  
  file.open(PREFS_FILENAME, O_RDWR); // Open the prefs-file in read-write mode
  
  file.seekSet(0); // Ensure we're on the first byte of the file
  file.read(cbuf, PREFS_ITEMS_1); // Read the old prefs-values
  
  for (byte i = 0; i < PREFS_ITEMS_1; i++) { // For every byte in the prefs-file...
    if (buf[i] != cbuf[i]) { // If the new byte doesn't match the old byte...
      file.seekSet(i); // Go to the byte's location
      file.write(buf[i]); // Replace the old byte
    }
  }
  
  file.close(); // Close the prefs-file
  
  char name[8];
  getFilename(name, SONG); // Get the name of the current song-file
  
  file.open(name, O_RDWR); // Reopen the current song-file
  
  writeTempo(); // Update the savefile's header-bytes for TEMPO if they don't match current values
  updateNonMatch(FILE_RANGE_BYTE, RANGE); // Update the savefile's header-byte for RANGE
  
}

// Load the contents of the prefs-file, and put its contents into global variables
void loadPrefs() {
  
  byte buf[PREFS_ITEMS_2]; // Make a buffer that will contain all the prefs
  
  if (sd.exists(PREFS_FILENAME)) { // If the prefs-file exists...
    
    file.open(PREFS_FILENAME, O_READ); // Open the prefs-file in read-mode
    file.read(buf, PREFS_ITEMS_1); // Read all the prefs-bytes into the buffer
    
    // Assign the buffer's-worth of pref-bytes to their respective global-vars
    MODIFY = buf[0];
    OCTAVE = buf[1];
    VELO = buf[2];
    HUMANIZE = buf[3];
    CHAN = buf[4];
    QRESET = buf[5];
    QUANTIZE = buf[6];
    DURATION = buf[7];
    REPEAT = buf[8];
    OFFSET = buf[9];
    SONG = buf[10];
    CLOCKSOURCE = buf[11];
    GRIDCONFIG = buf[12];
    CHANCE = buf[13];
    // reserved = buf[14];
    // reserved = buf[15];
    // reserved = buf[16];
    
  } else { // Else, if the prefs-file doesn't exist...
    
    makePrefBuf(buf); // Fill the prefs-data buffer with the current user-prefs
    
    file.createContiguous(PREFS_FILENAME, PREFS_ITEMS_1); // Create a contiguous prefs-file
    file.close(); // Close the newly-created file
    
    file.open(PREFS_FILENAME, O_WRITE); // Open the new prefs-file in write-mode
    file.write(buf, PREFS_ITEMS_1); // Write the pref vars into the file
    
  }
  
  file.close(); // Close the prefs file, regardless of whether it already exists or was just created
  
}

// Load a given song, or create its savefile if it doesn't exist
void loadSong(byte slot) {
  
  if (file.isOpen()) { // If a savefile is already open...
    file.close(); // Close the current savefile
  }
  
  resetAll(); // Reset all sequence-data (this leaves the GLOBAL CUE the same)
  
  HCHAN[0] = 255; // Set HOCKET to "inactive"
  SCOUNT = 0; // Clear any residual SHIFT-countdown
  
  char name[8]; // Create a buffer to hold the new savefile's filename
  getFilename(name, slot); // Get the name of the target song-slot's savefile
  
  file.open(name, O_RDWR); // Open the new savefile
  
  getTempo(); // Read the TEMPO information from the current savefile
  getTimeSignature(); // Get the song's default TIMESIG1 and TIMESIG2 values, and then use them to compute a TIMESIG3 value
  getRange(); // Read the RANGE information from the current savefile
  
  file.seekSet(FILE_SQS_START); // Go to the file-header's SEQ-SIZE block
  file.read(STATS, 48); // Read every seq's SIZE-byte
  
  ACTIVE_SEQS = 0; // Empty out the var that tracks how many seqs are playing
  
  // This for-loop will activate any seqs that have been flagged as ACTIVE ON LOAD in the file's header-block.
  for (byte i = 0; i < 48; i++) { // For each seq...
    if (
      (STATS[i] & 64) // If the seq is flagged as ACTIVE ON LOAD...
      && (ACTIVE_SEQS < MAX_ACTIVE_SEQS) // And the number of possible active-seqs hasn't reached its maximum yet...
    ) {
      POS[i] = CURTICK % seqLen(i); // Set the seq's POSITION to match the GLOBAL CUE POINT, or wrap it if the seq-length is short
      STATS[i] |= 128; // Activate the sequence
      ACTIVE_SEQS++; // Increase the tracking-var for number of active seqs
    }
  }
  
  SONG = slot; // Set the currently-active SONG-position to the given save-slot
  
  PAGE = 0; // Reset the PAGE status to PAGE 1
  
  // Write the current relevant global vars into P.DAT.
  // This is near the end of the function to ensure the new SONG value is stored as well.
  writePrefs();
  
  LOADHOLD = TIMESIG2 * 3; // Give LOADHOLD 3 beats worth of decay, to display the song-number for that long
  
  TO_UPDATE = 255; // Flag entire GUI for an LED-update
  
}
