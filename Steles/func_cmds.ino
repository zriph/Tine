

// Toggle whether a sequence will automatically start playing on FILE-LOAD
void actOnLoadCmd(__attribute__((unused)) byte col, __attribute__((unused)) byte row) {
  STATS[RECORDSEQ] ^= 64; // Flip the current RECORDSEQ's "ACTIVE ON LOAD" bit
  TO_UPDATE |= 1; // Flag the topmost row for updating
}

// Arm or disarm RECORDNOTES
void armCmd(__attribute__((unused)) byte col, __attribute__((unused)) byte row) {

  RECORDNOTES ^= 1; // Arm or disarm the RECORDNOTES flag
  
  GLYPHVIS = 0; // Clear any note-glyphs that might otherwise momentarily be visible from before RECORDNOTES was armed
  
  TO_UPDATE |= 248; // Flag the bottom 5 LED-rows for updating
  
}

// Parse a BASE NOTE press
void baseCmd(byte col, byte row) {
  BASENOTE = fullChange(col, row, BASENOTE, 0, 11); // Modify the BASENOTE value, clamped within a range of 0 to 11
  TO_UPDATE |= 1; // Flag the top LED-row for updating
}

// Parse a CHAN press
void chanCmd(byte col, byte row) {

  // Modify the CHAN value (temporarily stripped of its command-bits), clamped within a range of 0 to 15,
  // and then recombine it with its command-bits afterwards.
  CHAN = (CHAN & B11110000) | fullChange(col, row, CHAN & B00001111, 0, 15);
  
  TO_UPDATE |= 1; // Flag the top LED-row for updating

}

// Parse a CHANCE press
void chanceCmd(byte col, byte row) {
  
  CHANCE = fullChange(col, row, CHANCE, 0, 255); // Modify the CHANCE value, wrapping it around the 0-255 range
  
  TO_UPDATE |= 1; // Flag the top LED-row for updating
  
}

// Parse a CLOCK SOURCE press
void clockSourceCmd(byte col, byte row) {
  
  // If specifically toggling into CLOCKSOURCE="external" mode, disable REPEAT, since this mode can also represent step-sequencing
  REPEAT = CLOCKSOURCE ? REPEAT : 0;
  
  CLOCKSOURCE = fullChange(col, row, CLOCKSOURCE, 0, 0); // Modify the CLOCKSOURCE value, wrapping it around the limits of "byte" (0-255)
  
  resetTiming(); // Reset timing of all seqs and the global-cue, and send a SONG-POSITION POINTER if CLOCK SOURCE is internal
  
  TO_UPDATE |= 1; // Flag the top LED-row for updating
  
}

// Parse a COPY TRACK press:
// In order, view all commands from the current track of the selected sequence, and copy them into the current track of the RECORDSEQ.
// NOTE: This command is affected by the presence of active MODIFY bits!
void copyCmd(byte col, byte row) {
  
  TO_WRITE = 0; // Clear TO_WRITE, to prevent any possible conflicts with held-commands
  
  byte cseq = (col + (row << 2)) + ((!byte(BUTTONS & 1)) * 24); // Get the data-key of the seq-to-copy
  
  // Get the data-positions, in bytes, of both the seq-to-copy and the RECORDSEQ:
  // If TRACK 2 is active, offset the RECORDSEQ and copyseq's positions by 5 bytes;
  // If the seq-to-copy and the RECORDSEQ are the same seq, then set the seq-to-copy to the currently-inactive TRACK's position
  unsigned long cspos = FILE_BODY_START + (BYTES_PER_SEQ * cseq) + ((TRACK ^ (cseq == RECORDSEQ)) * 5);
  unsigned long rspos = FILE_BODY_START + (BYTES_PER_SEQ * RECORDSEQ) + (TRACK * 5);
  
  // For a number of tick-positions equal to that in the smaller of the two sequences...
  for (word i = 0; i < (min(seqLen(cseq), seqLen(RECORDSEQ)) * 10); i += 10) {
    
    file.seekSet(cspos + i); // Go to this tick's exact data-position in the seq-to-copy
    
    file.read(BUFWRITE, 5); // Read a single command's worth of bytes in the seq-to-copy
    
    sendMaxCmd(3, byte(i & 255)); // Send a bitwise fidget-value to the 3rd LED-row, to confirm that a command is being processed
    
    writeCommand(rspos + i); // Write the command into the RECORDSEQ at a position that corresponds to its position in the seq-to-copy
    
  }
  
  BLINK[TRACK] = TIMESIG2; // Start an LED-blink on the current TRACK for a TIMESIG-DIVISOR's-worth of ticks
  memset(GLYPH, 240 >> (TRACK << 2), 5); // Set a GLYPH that illuminates every LED on the current TRACK's side of the screen
  GLYPHVIS = TIMESIG2; // Hold the new GLYPH for a TIMESIG-DIVISOR's-worth of ticks
  TO_UPDATE |= 254; // Flag the bottom 7 LED-rows for updating
  
}

// Parse a DURATION press
void durationCmd(byte col, byte row) {
  DURATION = fullChange(col, row, DURATION, 0, 0); // Modify the DURATION value, wrapping it around the 0-255 range
  TO_UPDATE |= 1; // Flag the top LED-row for updating
}

// Parse an ERASE NOTE press
void eraseCmd(__attribute__((unused)) byte col, __attribute__((unused)) byte row) {
  
  if (REPEAT) { return; } // If REPEAT is active, ignore this command, and wait for the next tick to happen
  
  // Erase any notes within the RECORDSEQ's current QUANTIZE-QRESET-OFFSET-tick
  eraseTick(applyOffset(1, applyQuantize(POS[RECORDSEQ])));
  
  memset(BLINK, 1, 2); // Set a full-screen BLINK for 1 tick...
  TO_UPDATE |= 4; // ... and flag the 3rd LED-row to be updated.
  
}

// Parse a regular note-button press
void genericCmd(byte col, byte row) {
  
  // Parse an AUTO-DURATION input based on this button-press, with OCTAVE and GRIDCONFIG applied to its pitch
  parseAutoInput(0, (col * 6) + row, CHAN, modKeyPitch(col, row), VELO);
  
}

// Parse a GRIDCONFIG press
void gridConfigCmd(byte col, byte row) {
  GRIDCONFIG = fullChange(col, row, GRIDCONFIG, 0, GRID_TOTAL); // Modify the GRIDCONFIG value, clamping it within its limits
  TO_UPDATE |= 1; // Flag the top LED-row for updating
}

// Parse a HUMANIZE press
void humanizeCmd(byte col, byte row) {
  HUMANIZE = fullChange(col, row, HUMANIZE, 0, 127); // Modify the HUMANIZE value, clamping it within its limits
  TO_UPDATE |= 1; // Flag the top LED-row for updating
}

// Parse an INSERT REPEATING press
void insertRptCmd(byte col, byte row) {
  
  TO_WRITE = 0; // Clear TO_WRITE, to prevent any possible conflicts with held-commands
  
  BUFWRITE[0] = CHAN; // First held byte: Command-type and MIDI-CHANNEL
  BUFWRITE[1] = modKeyPitch(col, row); // Second held byte: Pitch (or corresponding data-payload-byte)
  // BUFWRITE[2] will be defined a few lines later, because it needs HUMANIZE applied on every insertion
  BUFWRITE[3] = DURATION; // Fourth held byte: Default auto-DURATION value
  BUFWRITE[4] = CHANCE; // Fifth held byte: The current user-entered CHANCE value
  
  // Get the RECORDSEQ's absolute data-position, plus a TRACK-based offset
  unsigned long rspos = FILE_BODY_START + (BYTES_PER_SEQ * RECORDSEQ) + (TRACK * 5);
  
  word oldpos = POS[RECORDSEQ]; // Save the RECORDSEQ's current position, so we can remember where we started traversing the seq
  
  do { // Do the following...
    
    if (isInsertionPoint()) { // If this is a QUANTIZE-QRESET-OFFSET-aligned insertion-point in the RECORDSEQ...
      
      BUFWRITE[2] = modVelo(VELO); // Third held byte: Velocity (or corresponding data-payload-byte), with HUMANIZE applied
      
      validateHeldBytes(); // Ensure that the command's payload-bytes are in the correct format for their command-type
      
      xorShift(); // Refresh the semirandom numbers, to supply modVelo() with new HUMANIZE values
      
      writeCommand(rspos + (POS[RECORDSEQ] * 10)); // Record the current held-TEMPO-command into the current RECORDSEQ-position
      
    }
    
    POS[RECORDSEQ]++; // Increment the RECORDSEQ's tick-position
    
  } while (POS[RECORDSEQ] <= seqLen(RECORDSEQ)); // ...And keep doing this until the RECORDSEQ's position has reached the sequence's end-point
  
  POS[RECORDSEQ] = oldpos; // Reset the sequence's position to where it was at the start of this command, to prevent desyncing from other seqs
  
  BLINK[TRACK] = TIMESIG2; // Start an LED-blink on the current TRACK for a TIMESIG-DIVISOR's-worth of ticks
  memset(GLYPH, 240 >> (TRACK << 2), 5); // Set a GLYPH that illuminates every LED on the current TRACK's side of the screen
  GLYPHVIS = TIMESIG2; // Hold the new GLYPH for a TIMESIG-DIVISOR's-worth of ticks
  TO_UPDATE |= 254; // Flag the bottom 7 LED-rows for updating
  
}

// Parse a MANUAL-DURATION press
void manualCmd(byte col, byte row) {
  
  // Parse a MANUAL-DURATION input based on this button-press, with OCTAVE and GRIDCONFIG applied to its pitch
  parseManualInput(0, (col * 6) + row, CHAN, modKeyPitch(col, row), VELO);
  
}

// Parse a MODIFY press
void modifyCmd(byte col, byte row) {
  
  // Flip the MODIFY-bit that corresponds to the row-and-column that were pressed:
  // If row 0, 2, or 4, flip the CHAN bit.
  // If row 1, 3, or 5, then flip either the PITCH, VELO, DUR, or CHANCE bit, corresponding to the pressed column.
  MODIFY ^= 1 << ((row & 1) ? (3 - col) : 4);
  
  TO_UPDATE |= 1; // Flag the topmost row for updating
  
}

// Parse a MOVE BY TICK press:
// Move the current position by a number of positive or negative ticks, to the power of the given grid-row, multiplied by QUANTIZE.
// Note: This command is designed to be used for step-sequencing,
// ...in situations where CLOCKSOURCE is set to "external" but no external clock is actually being received.
void moveByTickCmd(byte col, byte row) {
  
  // Automatically disable the REPEAT/arpeggiation system,
  // since it would otherwise stop MIDI-commands from being recorded when MOVE BY TICK is being used for step-sequencing.
  REPEAT = 0;
  
  // Get the distance, in positive or negative ticks, between the current tick and the new tick-position:
  int change = int(col & 2) - 1; // Get whether the keypress was in a left-side column (-1) or a right-side column (1)
  if (row) { // If this was in the bottom 5 rows...
    change *= int(32 >> row) * QUANTIZE; // Get a multiple of QUANTIZE that corresponds to the row, and make it positive or negative
  } else { // Else, if this was in the topmost row...
    change *= (col % 3) ? 1 : (QUANTIZE >> 1); // Get a "change" value of either 1, [QUANTIZE / 2], or their negative counterparts
  }
  
  if (change < 0) { // If the new tick-position is going to be "earlier" than the old tick-position (and note: "change" can't be "0")...
    while (SUST_COUNT) { // While any notes are still being sustained...
      endSustain(0); // Halt the topmost sustained note in the SUSTAIN system
    }
  } else { // Otherwise, if the new tick-position is going to be "later"...
    for (byte i = 0; i < change; i++) { // For every tick between the old tick-position and the new tick-position...
      processSustains(); // Process one tick's worth of duration for all notes in the SUSTAIN system
    }
  }
  
  clearBlink(); // Clear any current BLINKs/GLYPHs
  
  shiftCuePoint(change); // Shift the GLOBAL CUE by the given amount, wrapping it around its TIMESIG-based limits
  shiftPlayingSeqs(change); // Shift every playing-seq's position by the given amount, wrapping them around their SIZE-based limits
  
  // Iterate all seqs, processing any commands within their current ticks, but NOT advancing any seq-positions.
  // (Note: this is called from here to give an audible response to the tick-position after it has been changed)
  iterateSeqs(0);
  
  // Flag the top two LED-rows for updating, since the exact binary tick-position can take up two rows of LEDs...
  // Also flag the third row (BLINK-row) for updating, since BLINKs need to update when this command is still being held
  TO_UPDATE |= 7;
  
}

// Parse an OCTAVE press
void octaveCmd(byte col, byte row) {
  OCTAVE = fullChange(col, row, OCTAVE, 0, 10); // Modify the OCTAVE value, clamping it within its limits
  TO_UPDATE |= 1; // Flag the top LED-row for updating
}

// Parse an OFFSET press
void offsetCmd(byte col, byte row) {
  
  if (row) { // If this won't be a bitshift command...
    
    // First, shift OFFSET's value into a fully positive range. [OFFSET + 48]
    // Then, modify the temp-OFFSET value, in a range of 0 to 96. [fullChange()]
    // (Note: The upper value for OFFSET should be 96, not 95, because 48 is treated as "0" here)
    // Finally, convert the temporary-offset back into char-range, and then put its value into OFFSET. [char(...) - 48]
    OFFSET = char(fullChange(col, row, OFFSET + 48, 0, 96)) - 48;
    
  } else { // Else, if this *will* be a bitshift command...
    
    // First, get OFFSET as a positive integer, turning any negative value into its positive equivalent. [abs(OFFSET)]
    // Then, modify that positive temp-value, in a range of 0 to 48. [fullChange()]
    // Finally, turn the temp-value negative if OFFSET started out negative, then set OFFSET to that value.
    OFFSET = char(fullChange(col, row, abs(OFFSET), 0, 48)) * ((OFFSET < 0) ? -1 : 1);
    
  }
  
  TO_UPDATE |= 1; // Flag the top LED-row for updating
  
}

// Parse a SHIFT POSITION press
void posCmd(byte col, byte row) {
  
  // Get the distance, in positive or negative ticks, between the current tick and the new tick-position,
  // ...in a multiple of the current measure-size.
  int change = int(32 >> row) * (int(col & 2) - 1) * TIMESIG3;
  
  shiftCuePoint(change); // Shift the GLOBAL CUE by the given amount, wrapping it around its TIMESIG-based limits
  
  if (EDITMODE) { // If EDITMODE is active... (This check is necessary because posCmd() is called from both EDIT MODE and PLAY MODE)
    shiftPlayingSeqs(change); // Shift every playing-seq's position by the given amount, wrapping them around their SIZE-based limits
  }
  
  TO_UPDATE |= 3; // Flag the top 2 LED-rows for updating.
  
}

// Parse a QRESET press
void qrstCmd(byte col, byte row) {
  QRESET = fullChange(col, row, QRESET, 0, 0); // Modify the QRESET value, wrapping it around the 0-255 range
  TO_UPDATE |= 1; // Flag the top LED-row for updating
}

// Parse a QUANTIZE press
void quantizeCmd(byte col, byte row) {
  QUANTIZE = fullChange(col, row, QUANTIZE, 0, 0); // Modify the QUANTIZE value, wrapping it around the 0-255 range
  QUANTIZE |= !QUANTIZE; // If the new QUANTIZE value is 0, set it to 1 instead, so the mechanisms that expect it to be >=1 don't break
  TO_UPDATE |= 1; // Flag the top LED-row for updating
}

// Parse a REPEAT press
void repeatCmd(byte col, byte row) {
  REPEAT = fullChange(col, row, REPEAT, 0, 3); // Modify the REPEAT value, clamping it within its limits
  TO_UPDATE |= 1; // Flag the top LED-row for updating
}

// Parse a SEQ-SIZE press
void sizeCmd(byte col, byte row) {
  
  // First, isolate the seq's SIZE-bits from the rest of its STATUS value, and add 1 to them. [(STATS[...] & B00111111) + 1]
  // Then, modify the temp SIZE value, clamping it within its acceptable range. [fullChange(...)]
  // Then, subtract 1 from the temp SIZE value, returning it to its original 0-indexed range. [fullChange(...) - 1]
  // Then, recombine the modified temp SIZE value with the seq's other STATUS bits. [(STATS[...] & B11000000) | ...]
  // Finally, make that modified composite-value into the new STATS value for the current RECORDSEQ.
  STATS[RECORDSEQ] = (STATS[RECORDSEQ] & B11000000) | (fullChange(col, row, (STATS[RECORDSEQ] & B00111111) + 1, 1, 64) - 1);
  
  resetTiming(); // Reset all timing of all seqs and the global cue-point, and send a SONG-POSITION POINTER if CLOCK SOURCE is internal
  
  TO_UPDATE |= 3; // Flag the top 2 LED-rows for updating
  
}

// Parse a TEMPO-press
void tempoCmd(byte col, byte row) {
  
  // Convert the keypress' column and row into a larger var-change value,
  // which doubles on every column and row, instead of every row.
  int bc = int((2048 - ((col % 3) ? 1024 : 0)) >> (row * 2)) * (char(col & 2) - 1);
  
  long nt = long(TEMPO) + bc; // Get a new TEMPO-value that has the big-change-value added to it
  nt = (nt < TEMPO_LIMIT_LOW) ? TEMPO_LIMIT_LOW : nt; // If the new value is below the lower-limit, correct it
  nt = (nt > TEMPO_LIMIT_HIGH) ? TEMPO_LIMIT_HIGH : nt; // ^ Same, but for high-limit
  
  TEMPO = word(nt); // Set TEMPO to the updated-and-corrected tempo-value
  
  TO_UPDATE |= 3; // Flag the top 2 LED-rows for updating
  
}

// Parse a TRACK-press
void trackCmd(__attribute__((unused)) byte col, __attribute__((unused)) byte row) {
  
  TRACK ^= 1; // Toggle the TRACK value to either 0 or 1
  
  clearBlink(); // Clear any current BLINKs/GLYPHs
  
  if (CLOCKSOURCE) { // If CLOCKSOURCE is "external" / "step-sequencing mode"...
    // Iterate all seqs, processing any commands within their current ticks, but NOT advancing any seq-positions.
    // (Note: this is called from here to give an audible response after TRACK has been changed)
    iterateSeqs(0);
  }
  
  TO_UPDATE |= 253; // Flag the top row, BLINK row, and 5 bottom rows, for updating
  
}

// Parse a TIME-SIGNATURE DIVIDEND press
void tsDividendCmd(byte col, byte row) {
  
  TIMESIG1 = fullChange(col, row, TIMESIG1, 2, 8); // Modify the TIMESIG1 value, clamping it to within its limits
  
  tsUpdateWrap(); // Update TIMESIG3, then wrap the CURTICK around the number of total measures, to keep it from going out-of-bounds
  
  TO_UPDATE |= 1; // Flag the top LED-row for updating
  
}

// Parse a TIME-SIGNATURE DIVISOR press
void tsDivisorCmd(byte col, byte row) {
  
  TIMESIG2 = fullChange(col, row, TIMESIG2, 7, 12); // Modify the TIMESIG2 value, clamping it to within its limits
  
  tsUpdateWrap(); // Update TIMESIG3, then wrap the CURTICK around the number of total measures, to keep it from going out-of-bounds
  
  TO_UPDATE |= 1; // Flag the top LED-row for updating
  
}

// Parse an UPPER COMMAND BITS press
void upperBitsCmd(byte col, byte row) {
  
  // First, bitshift the UPPER CHAN bits right by 4, so they can be modified as integers. [CHAN >> 4]
  // Then, change the temp UPPER CHAN value by the given amount, clamping it within its limits. [fullChange(...)]
  // Then, bitshift the temp UPPER CHAN value left again, to its correct bitwise position. [fullChange() << 4]
  // Then, recombine the MIDI CHANNEL bits with the temp modified UPPER CHAN bits. [(CHAN & B00001111) | ...]
  // Finally, set the CHAN value to the modified composite value.
  CHAN = (CHAN & B00001111) | (fullChange(col, row, CHAN >> 4, UPPER_BITS_LOW >> 4, UPPER_BITS_HIGH >> 4) << 4);
  
  TO_UPDATE |= 1; // Flag the top LED-row for updating
  
}

// Parse a VELOCITY press
void veloCmd(byte col, byte row) {
  VELO = fullChange(col, row, VELO, 0, 127); // Modify the VELO value, clamping it to within its limits
  TO_UPDATE |= 1; // Flag the top LED-row for updating
}

// Parse a null command
void xCmd(__attribute__((unused)) byte col, __attribute__((unused)) byte row) {}


