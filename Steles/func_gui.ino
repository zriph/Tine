

// Display the number of a newly-loaded savefile
void displayLoadNumber() {
  
  byte c1 = SONG % 10; // Get the ones digit
  byte c2 = (SONG / 10) % 10; // Get the tens digit
  
  c1 += c1 << 2; // Multiply the ones digit, and tens digit, each by 5, to prep them for retrieving values from NUMBER_GLYPHS
  c2 += c2 << 2; // ^
  
  for (byte i = 0; i <= 4; i++) { // For each of the bottom 5 rows of the GUI...
    sendMaxCmd(
      i + 4, // Set this row...
      (pgm_read_byte_near(NUMBER_GLYPHS + c1 + i) >> 4) // ...To a composite of multiple digits, shifted in position accordingly
      | pgm_read_byte_near(NUMBER_GLYPHS + c2 + i) // ^
    );
  }
  
}

// Get the LED-value for a single seq, adjusted based on whether it's active and whether it has a dormant cue-command
byte getSeqVal(byte s, byte b) {
  
  // Get the seq's activity-bit. If it is ON, then offset it to the given bit-location; else get rid of that value completely
  byte b2 = (STATS[s] & 128) ? b : 0;
  
  // If this seq has any pending cued commands, then:
  // Check whether CURTICK is evenly divided by the time-sig-divisor. If so, send the original "b" value; otherwise, invert its single bit.
  // But if the seq doesn't have any pending CUE-commands, send the original "b" value regardless.
  return CMD[s] ? (b2 ^ ((CURTICK % TIMESIG2) ? 0 : b)) : b2;
  
}

// Get the SEQUENCE-ACTIVITY LED-values for a given GUI row
byte getRowSeqVals(byte r1) {
  byte r2 = r1 + 24; // Get the row-start-point for the corresponding seqs on the second PAGE
  return ( // Return the row's PLAYING info, from both pages, as an LED-row's worth of bits
    getSeqVal(r1, 128)
    | getSeqVal(r1 | 1, 64)
    | getSeqVal(r1 | 2, 32)
    | getSeqVal(r1 | 3, 16)
    | getSeqVal(r2, 8)
    | getSeqVal(r2 | 1, 4)
    | getSeqVal(r2 | 2, 2)
    | getSeqVal(r2 | 3, 1)
  );
}

// Clear any current BLINKs/GLYPHs
void clearBlink() {
  GLYPHVIS = 0; // Clear any remaining ticks in the GLYPHVIS timer
  memset(BLINK, 0, 2); // Clear any remaining ticks in both BLINK array entries
}

// Display every LED-row containing GLYPH data, based on the contents of the GLYPH vars
void displayGlyph() {
  
  if ((GLYPH[0] & 240) == 144) { // If the glyph is for a NOTE-ON...
    
    byte gmod = GLYPH[1] % 12; // Get a "de-octaved" note-value from the second GLYPH byte...
    gmod += gmod << 2; // ...And multiply it by 5, to prep it to select data from GLYPH_NOTE. (Each glyph is 5 bytes large)
    
    if (TRACK) { // If this is a NOTE command on TRACK 2...
      
      // Get and send the first glyph-row, shifted right, combined with the note's CHANNEL
      sendMaxCmd(4, (pgm_read_byte_near(GLYPH_NOTE + gmod) >> 5) | (GLYPH[0] << 4));
      
      // Get and send the second glyph-row, shifted right, combined with the note's truncated VELOCITY
      sendMaxCmd(5, (pgm_read_byte_near(GLYPH_NOTE + gmod + 1) >> 5) | (GLYPH[2] & 240));
      
      // Get and send the third glyph-row, shifted right, combined with the 1st half of the note's DURATION
      sendMaxCmd(6, (pgm_read_byte_near(GLYPH_NOTE + gmod + 2) >> 5) | (GLYPH[3] & 240));
      
      // Get and send the fourth glyph-row, shifted right, combined with the 2nd half of the note's DURATION
      sendMaxCmd(7, (pgm_read_byte_near(GLYPH_NOTE + gmod + 3) >> 5) | (GLYPH[3] << 4));
      
      // Get and send the fifth glyph-row, shifted right, combined with the note's truncated CHANCE
      sendMaxCmd(8, (pgm_read_byte_near(GLYPH_NOTE + gmod + 4) >> 5) | (GLYPH[4] & 240));
      
    } else { // Else, if this is a NOTE command on TRACK 1...
      
      // Display the note-glyph, with extras:
      sendMaxCmd(4, pgm_read_byte_near(GLYPH_NOTE + gmod) | (GLYPH[0] & 15)); // Display 1st row, combined with note's CHANNEL
      sendMaxCmd(5, pgm_read_byte_near(GLYPH_NOTE + gmod + 1) | (GLYPH[2] >> 3)); // Display 2nd row, combined with truncated VELOCITY
      sendMaxCmd(6, pgm_read_byte_near(GLYPH_NOTE + gmod + 2) | (GLYPH[3] >> 4)); // Display 3rd row, combined with 1st half DURATION
      sendMaxCmd(7, pgm_read_byte_near(GLYPH_NOTE + gmod + 3) | (GLYPH[3] & 15)); // Display 4th row, combined with 2nd half DURATION
      sendMaxCmd(8, pgm_read_byte_near(GLYPH_NOTE + gmod + 4) | (GLYPH[4] >> 4)); // Display 5th row, combined with truncated CHANCE
      
    }
    
  } else { // Else, if this is a non-NOTE command...
    
    sendMaxCmd(4, GLYPH[0]); // Send the command's CMD/CHAN byte
    sendMaxCmd(5, GLYPH[1]); // Send the command's first data-byte
    sendMaxCmd(6, GLYPH[2]); // Send the command's second data-byte
    sendMaxCmd(7, GLYPH[3]); // Send the command's third data-byte
    sendMaxCmd(8, GLYPH[4]); // Send the command's CHANCE-byte
    
  }
  
}

// Update the bottom LED-rows for EDIT MODE
void updateRecBottomRows(byte ctrl) {
  
  unsigned int gly; // Will contain the memory-position from which to read the current situation's GLYPH
  
  if (ctrl) { // If any command-buttons are held...
    
    gly = word(ctrl); // Copy over the current held ctrl-command value...
    gly += (gly << 2) + ((unsigned int)&GLYPHS); // ...And multiply it by 5, and add that to the memory-position of the GLYPHs array
    
  } else { // Otherwise, if no command-buttons are held...
    
    // Flag it to contain either an "EDIT MODE" glyph, or a "RECORDNOTES is armed" glyph, specific to the current TRACK
    gly = (unsigned int)(RECORDNOTES ? (TRACK ? GLYPH_EDITREC2 : GLYPH_EDITREC1) : (TRACK ? GLYPH_EDITMODE2 : GLYPH_EDITMODE1));
    
  }
  
  byte putoffset = (!(GLYPHVIS || ctrl)) && OFFSET; // Get whether to apply an offset-based display modification or not
  
  byte row = 0; // Will hold the binary value to be sent to the row's LEDs
  
  for (byte i = 0; i <= 4; i++) { // For each of the bottom 5 GUI rows...
    
    if (TO_UPDATE & (8 << i)) { // If the row is flagged for an update...
      
      // Read the given row in the currently-active glyph, or wrap the read-rows around if OFFSET is enabled
      row = pgm_read_byte_near(gly + (putoffset ? ((i + 3) % 5) : i));
      
      if (ctrl == BINARY_ARMREC) { // If RECORDNOTES is held...
        row |= RECORDNOTES ? 15 : 0; // Add a blaze to the row if RECORDNOTES is armed
      } else if (ctrl == BINARY_ERASE) { // Else, if ERASE NOTES is held...
        row &= 192 | (RECORDNOTES ? 63 : 0); // Add right-side exclamation-points if RECORD NOTES is active
      } else if (ctrl == BINARY_ERASEINVERT) { // Else, if ERASE INVERSE NOTES is held...
        row &= 63 | ((RECORDNOTES && REPEAT) ? 192 : 0); // Add a left-side exclamation-point if RECORDNOTES and REPEAT are active
      } else if (!ctrl) { // Else, if no control-command is being held...
        byte comp = MODIFY ? pgm_read_byte_near(GLYPH_QUESTION_MARK1 + i) : 0; // Get a question-mark if any MODIFY-bits are on
        comp |= (CHANCE != 255) ? pgm_read_byte_near(GLYPH_QUESTION_MARK2 + i) : 0; // Get reverse-question-mark for low CHANCE values
        row |= comp << (TRACK * 5); // Display the question-mark(s), bit-shifted left by 5 if TRACK 2 is active
      }
      
      sendMaxCmd(i + 4, row); // Set the LED-row based on the given bitwise data
      
    }
    
  }
  
}

// Update the bottom LED-rows for PLAY-MODE
void updatePlayBottomRows(byte ctrl) {

  byte num = 0; // Will hold a magic number based on which LOAD command is being held, if a LOAD command is indeed being held
  
  byte isload = // Check whether any of the LOAD commands are being held, and store the resulting boolean
    (ctrl == BINARY_LOAD0)
    | (ctrl == BINARY_LOAD1)
    | (ctrl == BINARY_LOAD2)
    | (ctrl == BINARY_LOAD3);
    
  if (isload) { // If a LOAD command is held...
    num = (ctrl & 14) >> 1; // Generate a magic number that will display a glyph corresponding to its LOAD command
    num -= num >= 4; // ^
  }
  
  for (byte i = 3; i <= 7; i++) { // For each of the bottom 5 GUI rows...
    if (TO_UPDATE & (1 << i)) { // If the row is flagged for an update...
      if (ctrl == BINARY_SHIFTPOS) { // If a SHIFT POSITION command is held...
        sendMaxCmd(i + 1, pgm_read_byte_near(GLYPHS + 12 + i)); // Display a line from the SHIFT POSITION glyph
      } else if (ctrl == BINARY_RANGE) { // Else, if a RANGE command is held...
        sendMaxCmd(i + 1, pgm_read_byte_near(GLYPH_RANGE + (i - 3))); // Display a line from the RANGE glyph
      } else if (isload) { // Else, if a LOAD command is being held...
        // Display a line from the LOAD glyph, combined with a line from a numerical glyph, based on the held LOAD-command
        sendMaxCmd(i + 1, pgm_read_byte_near(GLYPH_LOAD + (i - 3)) | (pgm_read_byte_near(NUMBER_GLYPHS + (i - 3) + (num * 5)) >> 4));
      } else { // Else, if a regular PLAY MODE command is held...
        // Get a row-start value that corresponds to the seq-array positions; and display a row of SEQ info
        sendMaxCmd(i + 1, getRowSeqVals((i - 2) << 2));
      }
    }
  }

}

// Update the GUI based on update-flags that have been set by the current tick's events
void updateGUI() {
  
  byte ctrl = BUTTONS & B00111111; // Get the control-column buttons' activity
  
  if (TO_UPDATE & 1) { // If the first row is flagged for a GUI update...
    
    if (EDITMODE) { // If EDIT MODE is active...
      
      // Send a value to the top LED-row that is generated from a GuiFunc,
      // which itself corresponds to the current "ctrl" value (0-63),
      // which represents the current held ctrl-buttons.
      sendMaxCmd(1, ((GuiFunc) pgm_read_word_near(&GUIFUNCS[ctrl])) () );
      
    } else { // Else, if PLAY MODE is active...
      
      // Display a composite of the current GLOBAL CUE, RANGE, and PAGE values
      sendMaxCmd(1, (EDITMODE ? 0 : ((!PAGE) - 1)) ^ ((128 >> (CURTICK / TIMESIG3)) | (128 >> (RANGE - 1))));
      
    }
    
  }
  
  if (TO_UPDATE & 2) { // If the second row is flagged for a GUI update...
    
    if (EDITMODE && (ctrl == BINARY_TEMPO)) { // If a TEMPO command is being held in EDITMODE...
      
      sendMaxCmd(2, byte(TEMPO & 255)); // Send the second half of the current TEMPO word
      
    } else if (EDITMODE && (ctrl == BINARY_MOVEBYTICK)) { // Else, if a MOVE BY TICK command is being held in EDITMODE...
      
      sendMaxCmd(2, byte(POS[RECORDSEQ] & 255)); // Send the second half of the RECORDSEQ's current tick-position
      
    } else { // Otherwise...
      
      // Update the second LED-row based on the current musical time-counting position in the global measure
      sendMaxCmd(2, pgm_read_byte_near(QUAVERDISPLAY + (byte(CURTICK % TIMESIG3) / TIMESIG2)));
      
    }
    
  }
  
  if (TO_UPDATE & 4) { // If the third row is flagged for a GUI update...
    
    // In EDIT MODE, send whatever active BLINKs there are, or an empty row. In PLAY MODE, send the topmost row of seq-indicators.
    sendMaxCmd(3, EDITMODE ? ((BLINK[0] ? 240 : 0) | (BLINK[1] ? 15 : 0)) : getRowSeqVals(0));
    
  }
  
  if (TO_UPDATE & 248) { // If any of the bottom 5 rows are flagged for a GUI update...
    
    if (
      EDITMODE // If EDIT MODE is active...
      && GLYPHVIS // And any GLYPH is active...
      && (!ctrl) // And no ctrl-buttons are held...
    ) {
      
      displayGlyph(); // Display the current GLYPH
      
    } else { // Else, if this is PLAY MODE, or any ctrl-buttons are held, or no GLYPHs are active, update the bottom 6 rows of LEDs:
      
      if (LOADHOLD) { // If a number from a just-loaded savefile is being held on screen...
        displayLoadNumber(); // Display the number of the newly-loaded savefile
      } else if (EDITMODE) { // If EDIT MODE is active...
        updateRecBottomRows(ctrl); // Update the bottom-rows for EDIT MODE
      } else { // Else, if PLAY MODE is actve...
        updatePlayBottomRows(ctrl); // Update the bottom LED-rows for PLAY MODE
      }
      
    }
    
  }
  
  TO_UPDATE = 0; // Clear all TO_UPDATE flags
  
}

