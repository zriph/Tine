

// Initialize communications with the MAX72** chip, on regular digital pins.
void maxInitialize() {
  
  DDRD |= B11100000; // Set the Data, Clk, and CS pins as outputs
  
  sendMaxCmd(15, 0); // Conduct a display-test
  sendMaxCmd(11, 7); // Set scanlimit to max
  sendMaxCmd(9, 0); // Decode is done in source
  
  // Clear all LED-rows
  sendMaxCmd(1, 0);
  sendMaxCmd(2, 0);
  sendMaxCmd(3, 0);
  sendMaxCmd(4, 0);
  sendMaxCmd(5, 0);
  sendMaxCmd(6, 0);
  sendMaxCmd(7, 0);
  sendMaxCmd(8, 0);
  
  sendMaxCmd(12, 0); // Shutown mode: shutdown
  sendMaxCmd(12, 1); // Shutown mode: startup
  sendMaxCmd(10, 15); // LED intensity: high brightness (range is 0 to 15)
  
}

// Send a command to the MAX72** chip, while toggling the CS pin's data-latch to let the command through.
// The latch must be toggled for each command, or else they won't work. So this function toggles the latch every time.
void sendMaxCmd(volatile byte cmd, volatile byte d) {
  PORTD &= B10111111; // Set the CS pin low (data latch)
  shiftOut(5, 7, MSBFIRST, cmd); // Send the command's opcode
  shiftOut(5, 7, MSBFIRST, d); // Send the command's 1-byte LED-data payload
  PORTD |= B01000000; // Set the CS pin high (data latch)
}
