

// Scan a key-button, checking for changes in state and then reacting to any that are found
void scanButton(byte col, byte row, byte bpin) {
  
  byte bpos = (col * 6) + row; // Get the button's bit-position for use with the BUTTONS data
  byte bstate = ((row <= 2) ? PIND : PINC) & bpin; // Get the pin's input-state (low = ON; high = OFF)
  
  if ((!bstate) == ((BUTTONS >> bpos) & 1)) { return; } // If the button's state hasn't changed, exit the function
  
  byte oldcmds = byte(BUTTONS & B00111111); // Get the old COMMAND-row values
  BUTTONS ^= 1UL << bpos; // Flip the button's activity-bit into its new state
  if (bstate) { // If the button's new state is OFF...
    unassignKey(col, row, oldcmds); // Unassign any key-commands that might be based on this release-keystroke
  } else { // Else, the button's new state is ON, so...
    assignKey(col, row, oldcmds); // Assign any key-commands that might be based on this keystroke
  }
  
}

// Scan all keypad rows and columns, checking for changes in state
void scanKeypad() {
  
  PORTC &= B11111011; // Pulse the column's pin low, in port-bank C
  scanButton(0, 0, 16); // Scan a button for keystrokes
  scanButton(0, 1, 8); // Scan a button for keystrokes
  scanButton(0, 2, 4); // Scan a button for keystrokes
  scanButton(0, 3, 32); // Scan a button for keystrokes
  scanButton(0, 4, 16); // Scan a button for keystrokes
  scanButton(0, 5, 8); // Scan a button for keystrokes
  PORTC |= B00000100; // Raise the column's pin high, in port-bank C
  
  PORTC &= B11111101; // Pulse the column's pin low, in port-bank C
  scanButton(1, 0, 16); // Scan a button for keystrokes
  scanButton(1, 1, 8); // Scan a button for keystrokes
  scanButton(1, 2, 4); // Scan a button for keystrokes
  scanButton(1, 3, 32); // Scan a button for keystrokes
  scanButton(1, 4, 16); // Scan a button for keystrokes
  scanButton(1, 5, 8); // Scan a button for keystrokes
  PORTC |= B00000010; // Raise the column's pin high, in port-bank C
  
  PORTC &= B11111110; // Pulse the column's pin low, in port-bank C
  scanButton(2, 0, 16); // Scan a button for keystrokes
  scanButton(2, 1, 8); // Scan a button for keystrokes
  scanButton(2, 2, 4); // Scan a button for keystrokes
  scanButton(2, 3, 32); // Scan a button for keystrokes
  scanButton(2, 4, 16); // Scan a button for keystrokes
  scanButton(2, 5, 8); // Scan a button for keystrokes
  PORTC |= B00000001; // Raise the column's pin high, in port-bank C
  
  PORTB &= B11111101; // Pulse the column's pin low, in port-bank B
  scanButton(3, 0, 16); // Scan a button for keystrokes
  scanButton(3, 1, 8); // Scan a button for keystrokes
  scanButton(3, 2, 4); // Scan a button for keystrokes
  scanButton(3, 3, 32); // Scan a button for keystrokes
  scanButton(3, 4, 16); // Scan a button for keystrokes
  scanButton(3, 5, 8); // Scan a button for keystrokes
  PORTB |= B00000010; // Raise the column's pin high, in port-bank B
  
  PORTB &= B11111110; // Pulse the column's pin low, in port-bank B
  scanButton(4, 0, 16); // Scan a button for keystrokes
  scanButton(4, 1, 8); // Scan a button for keystrokes
  scanButton(4, 2, 4); // Scan a button for keystrokes
  scanButton(4, 3, 32); // Scan a button for keystrokes
  scanButton(4, 4, 16); // Scan a button for keystrokes
  scanButton(4, 5, 8); // Scan a button for keystrokes
  PORTB |= B00000001; // Raise the column's pin high, in port-bank B
  
}

