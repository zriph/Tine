


// Parse a CUE keystroke, and cue the given command
void cuePF(byte ctrl, byte col, byte row) {
  byte seq = col + (row << 2) + (PAGE * 24); // Get the sequence that corresponds to the given column and row
  CMD[seq] = ((ctrl & B00001110) << 4) | 2; // Cue an ON command for the seq, at the given cue-point
  RECORDSEQ = seq; // Set the most-recent-seq to the seq whose button was pressed
}

// Parse a CUE-OFF keystroke, and cue the given command
void cueOffPF(byte ctrl, byte col, byte row) {
  byte seq = col + (row << 2) + (PAGE * 24); // Get the sequence that corresponds to the given column and row
  if (STATS[seq] & 128) { // If the seq is playing...
    CMD[seq] = ((ctrl & B00001110) << 4) | 1; // Cue an OFF command for the seq, at the given cue-point
  }
  RECORDSEQ = seq; // Set the most-recent-seq to the seq whose button was pressed
}

// Parse an END SUSTAINS command
void endSustPF(__attribute__((unused)) byte ctrl, __attribute__((unused)) byte col, __attribute__((unused)) byte row) {

  if (!SUST_COUNT) { return; } // If there are no active sustains, exit the function
  
  for (byte i = 2; i < SUST_COUNT; i += 3) { // For every duration-byte in the sustain-data...
    SUST[i] = 127; // Replace the duration with a velocity-value (this shouldn't be necessary, but some synths act weird without it)
  }
  
  Serial.write(SUST, SUST_COUNT); // Send the now-fully-formed chain of NOTE-OFF commands
  
  memset(SUST, 0, SUST_COUNT); // Clear the SUST array's now-obsolete data
  SUST_COUNT = 0; // Set the sustain-count to 0
  
}

// Parse a LOAD SONG command
void loadPF(byte ctrl, byte col, byte row) {
  byte nums = (ctrl & B00001110) >> 1; // Get the raw "numbers" value, right-shifted
  nums -= nums == 4; // If the number is 4, then subtract 1
  loadSong(col + (row << 2) + (24 * nums)); // Load a new song from the LOAD-page that corresponds to the given number
}

// Parse an OFF command
void offPF(__attribute__((unused)) byte ctrl, byte col, byte row) {

  byte seq = col + (row << 2) + (PAGE * 24); // Get the sequence that corresponds to the given column and row, on the current PAGE
  
  CMD[seq] = 0; // Clear the seq's cued-commands
  POS[seq] = 0; // Reset the seq's tick-position
  
  if (STATS[seq] & 128) { // If the seq is playing...
    STATS[seq] &= 127; // Unset the seq's PLAYING-flag
    ACTIVE_SEQS--; // Decrease the number of currently-playing seqs
  }
  
  RECORDSEQ = seq; // Set the most-recent-seq to the seq whose button was just pressed
  
  TO_UPDATE |= 4 << row; // Flag the seq's corresponding row for updates
  
}

// Parse a PAGE command
void pagePF(__attribute__((unused)) byte ctrl, __attribute__((unused)) byte col, __attribute__((unused)) byte row) {
  PAGE ^= 1; // Toggle between page A and page B
  TO_UPDATE |= 1; // Flag the top LED-row for updates
}

// Parse a RANGE command
void rangePF(__attribute__((unused)) byte ctrl, byte col, byte row) {

  RANGE = fullChange(col, row, RANGE, 2, 8); // Modify the RANGE value, clamping it to within its limits
  
  CURTICK %= word(RANGE) * TIMESIG3; // Wrap the current tempo-tick around to fit the new RANGE value, if applicable
  
  TO_UPDATE |= 1; // Flag the top LED-row for updates

}

// Parse a SHIFT CUE command
void shiftCuePF(__attribute__((unused)) byte ctrl, byte col, byte row) {
  posCmd(col, row); // Shift the CURTICK value by calling posCmd(), which works in both PLAY MODE and EDIT MODE
}

// Parse a SLICE command
void slicePF(byte ctrl, byte col, byte row) {
  
  byte seq = col + (row << 2) + (PAGE * 24); // Get the sequence that corresponds to the given column and row
  
  RECORDSEQ = seq; // Set the most-recent-seq to the seq whose button was pressed
  
  if (CMD[seq] & B00000010) { // If the seq already contains a cued ON value...
    
    CMD[seq] = (CMD[seq] & B11100011) | ((ctrl & B00001110) << 1); // Add a slice-position to the cued ON command
    
  } else { // Else, if the seq has no cued ON value, then this is a pure beatslice. So...
    
    if (STATS[seq] & 128) { // If the seq is already playing...
      
      // Get the actual value of the user's slice-command, and send it to the seq-slicing function for the given seq.
      // Also, fill the seq's new slice with the number of ticks within its current slice, so it doesn't fall out of rhythm.
      applySlice(seq, (ctrl & B00001110) >> 1, 0);
      
    } else {
      
      if (ACTIVE_SEQS == MAX_ACTIVE_SEQS) { return; } // If the max number of seqs are active, exit the function
      
      ACTIVE_SEQS++; // If function hasn't exited, max-seqs hasn't been reached, so increase the number of currently-playing seqs
      
      // Get the actual value of the user's slice-command, and send it to the seq-slicing function for the given seq.
      // Also, fill the seq's new slice with the number of ticks within the current GLOBAL CUE slice.
      applySlice(seq, (ctrl & B00001110) >> 1, 1);
      
      STATS[seq] |= 128; // Set the seq's playing-flag to "on"
      
    }
    
  }
  
  TO_UPDATE |= 4 << row; // Flag the seq's corresponding LED-row for an update
  
}

// Parse a SWAP command
void swapPF(__attribute__((unused)) byte ctrl, byte col, byte row) {
  
  byte seq = col + (row << 2) + (PAGE * 24); // Get the sequence that corresponds to the given column and row
  
  if (SWAPLAST != 255) { // If SWAPLAST is any value other than 255 (aka "not empty")...
    
    byte laston = STATS[SWAPLAST] & 128; // See if the first-pressed seq is playing
    byte nexton = STATS[seq] & 128; // See if the second-pressed seq is playing
    
    if (laston || nexton) { // If one or both of the seqs are playing...
      
      // Swap the two seqs' "playing"-flags
      STATS[SWAPLAST] = (STATS[SWAPLAST] & 127) | nexton;
      STATS[seq] = (STATS[seq] & 127) | laston;
      
      // Swap both seqs' tick-positions, wrapping the larger seq around the size of the smaller seq if applicable
      word temp = POS[SWAPLAST];
      POS[SWAPLAST] = POS[seq] % seqLen(STATS[SWAPLAST]);
      POS[seq] = temp % seqLen(STATS[seq]);
      
      TO_UPDATE |= (4 << row) | (4 << ((SWAPLAST % 24) >> 2)); // Flag both seqs' LED-rows for GUI updates
      
    }
    
    // Swap both seqs' current CMD-data, regardless of whether the seqs are on or off
    byte tc = CMD[SWAPLAST];
    CMD[SWAPLAST] = CMD[seq];
    CMD[seq] = tc;
    
  }
  
  SWAPLAST = seq; // Set SWAPLAST to be the currently-pressed seq-button
  RECORDSEQ = seq; // Set the most-recent-seq to the currently-pressed seq-button too
  
}

// Parse a null command
void xPF(__attribute__((unused)) byte ctrl, __attribute__((unused)) byte col, __attribute__((unused)) byte row) {}


