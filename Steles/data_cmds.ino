


// Store pointers to EDIT MODE functions, in PROGMEM
// Note: These commands' corresponding binary values are reversed, compared to the values found in manual.txt
const CmdFunc COMMANDS[] PROGMEM = {
  genericCmd,     //  0, 000000: (REGULAR NOTE)
  armCmd,         //  1, 000001: RECORD NOTES (ARM RECORDING)
  trackCmd,       //  2, 000010: TRACK
  posCmd,         //  3, 000011: SHIFT POSITION
  veloCmd,        //  4, 000100: VELOCITY
  tempoCmd,       //  5, 000101: TEMPO
  humanizeCmd,    //  6, 000110: HUMANIZE
  moveByTickCmd,  //  7, 000111: MOVE BY TICK
  modifyCmd,      //  8, 001000: MODIFY
  gridConfigCmd,  //  9, 001001: GRID CONFIGURATION
  sizeCmd,        // 10, 001010: SEQ-SIZE
  xCmd,           // 11, 001011:
  quantizeCmd,    // 12, 001100: QUANTIZE
  xCmd,           // 13, 001101:
  xCmd,           // 14, 001110:
  copyCmd,        // 15, 001111: COPY TRACK FROM PAGE 1
  repeatCmd,      // 16, 010000: REPEAT
  baseCmd,        // 17, 010001: BASE-NOTE
  qrstCmd,        // 18, 010010: QUANTIZE-RESET
  xCmd,           // 19, 010011:
  upperBitsCmd,   // 20, 010100: UPPER CHAN BITS
  tsDividendCmd,  // 21, 010101: TIME-SIGNATURE DIVIDEND
  xCmd,           // 22, 010110:
  xCmd,           // 23, 010111:
  durationCmd,    // 24, 011000: DURATION
  xCmd,           // 25, 011001:
  xCmd,           // 26, 011010:
  xCmd,           // 27, 011011:
  xCmd,           // 28, 011100:
  xCmd,           // 29, 011101:
  xCmd,           // 30, 011110:
  xCmd,           // 31, 011111:
  manualCmd,      // 32, 100000: MANUAL-DURATION
  eraseCmd,       // 33, 100001: ERASE-NOTE
  offsetCmd,      // 34, 100010: OFFSET
  clockSourceCmd, // 35, 100011: CLOCK SOURCE
  actOnLoadCmd,   // 36, 100100: ACTIVE ON LOAD
  xCmd,           // 37, 100101:
  xCmd,           // 38, 100110:
  xCmd,           // 39, 100111:
  chanCmd,        // 40, 101000: CHANNEL
  xCmd,           // 41, 101001:
  tsDivisorCmd,   // 42, 101010: TIME-SIGNATURE DIVISOR
  xCmd,           // 43, 101011:
  xCmd,           // 44, 101100:
  xCmd,           // 45, 101101:
  insertRptCmd,   // 46, 101110: INSERT REPEATING
  xCmd,           // 47, 101111:
  octaveCmd,      // 48, 110000: OCTAVE
  xCmd,           // 49, 110001:
  xCmd,           // 50, 110010:
  xCmd,           // 51, 110011: (ERASE-INVERSE-NOTES is handled by other routines)
  xCmd,           // 52, 110100:
  xCmd,           // 53, 110101:
  xCmd,           // 54, 110110:
  xCmd,           // 55, 110111:
  chanceCmd,      // 56, 111000: CHANCE
  xCmd,           // 57, 111001:
  xCmd,           // 58, 111010:
  xCmd,           // 59, 111011:
  copyCmd,        // 60, 111100: COPY TRACK FROM PAGE 2
  xCmd,           // 61, 111101:
  xCmd,           // 62, 111110:
  xCmd            // 63, 111111: (TOGGLE EDIT MODE is handled by other routines)
};

// Store pointers to "top LED-row" GUI functions, in PROGMEM
// Note: These commands' corresponding binary values are reversed, compared to button-key.txt
const GuiFunc GUIFUNCS[] PROGMEM = {
  xTR,            //  0, 000000: (no command held: show default top-row)
  xTR,            //  1, 000001: (RECORD NOTES has default top-row)
  trackTR,        //  2, 000010: TRACK
  xTR,            //  3, 000011: (SHIFT POSITION has default top-row)
  veloTR,         //  4, 000100: VELOCITY
  tempoTR,        //  5, 000101: TEMPO
  humanizeTR,     //  6, 000110: HUMANIZE
  moveByTickTR,   //  7, 000111: MOVE BY TICK
  modifyTR,       //  8, 001000: MODIFY
  gridConfigTR,   //  9, 001001: GRID CONFIGURATION
  sizeTR,         // 10, 001010: SEQ-SIZE
  xTR,            // 11, 001011:
  quantizeTR,     // 12, 001100: QUANTIZE
  xTR,            // 13, 001101:
  xTR,            // 14, 001110:
  xTR,            // 15, 001111: (COPY TRACK FROM PAGE 1 has default top-row)
  repeatTR,       // 16, 010000: REPEAT
  baseTR,         // 17, 010001: BASE-NOTE
  qrstTR,         // 18, 010010: QUANTIZE-RESET
  xTR,            // 19, 010011:
  chanTR,         // 20, 010100: UPPER CHAN BITS (same top-row as CHANNEL)
  tsDividendTR,   // 21, 010101: TIME-SIGNATURE DIVIDEND
  xTR,            // 22, 010110:
  xTR,            // 23, 010111:
  durationTR,     // 24, 011000: DURATION
  xTR,            // 25, 011001:
  xTR,            // 26, 011010:
  xTR,            // 27, 011011:
  xTR,            // 28, 011100:
  xTR,            // 29, 011101:
  xTR,            // 30, 011110:
  xTR,            // 31, 011111:
  xTR,            // 32, 100000: (MANUAL-DURATION has default top-row)
  xTR,            // 33, 100001: (ERASE-NOTE has default top-row)
  offsetTR,       // 34, 100010: OFFSET
  clockSourceTR,  // 35, 100011: CLOCK SOURCE
  actOnLoadTR,    // 36, 100100: ACTIVE-ON-LOAD
  xTR,            // 37, 100101:
  xTR,            // 38, 100110:
  xTR,            // 39, 100111:
  chanTR,         // 40, 101000: CHANNEL
  xTR,            // 41, 101001:
  tsDivisorTR,    // 42, 101010: TIME-SIGNATURE DIVISOR
  xTR,            // 43, 101011:
  xTR,            // 44, 101100:
  xTR,            // 45, 101101:
  xTR,            // 46, 101110: (INSERT REPEATING has default top-row)
  xTR,            // 47, 101111:
  octaveTR,       // 48, 110000: OCTAVE
  xTR,            // 49, 110001:
  xTR,            // 50, 110010:
  xTR,            // 51, 110011: (ERASE-INVERSE-NOTES has default top-row)
  xTR,            // 52, 110100:
  xTR,            // 53, 110101:
  xTR,            // 54, 110110:
  xTR,            // 55, 110111:
  chanceTR,       // 56, 111000: CHANCE
  xTR,            // 57, 111001:
  xTR,            // 58, 111010:
  xTR,            // 59, 111011:
  xTR,            // 60, 111100: (COPY TRACK FROM PAGE 2 has default top-row)
  xTR,            // 61, 111101:
  xTR,            // 62, 111110:
  xTR             // 63, 111111: (TOGGLE EDIT MODE has default top-row)
};

// Store pointers to PLAY MODE functions, in PROGMEM
// Note: These commands' corresponding binary values are reversed, compared to button-key.txt
const PlayFunc PLAYCMDS[] PROGMEM = {
  slicePF,        //  0, 000000: SLICE 0
  pagePF,         //  1, 000001: PAGE
  slicePF,        //  2, 000010: SLICE 1
  shiftCuePF,     //  3, 000011: SHIFT CUE
  slicePF,        //  4, 000100: SLICE 2
  swapPF,         //  5, 000101: SWAP
  slicePF,        //  6, 000110: SLICE 3
  xPF,            //  7, 000111:
  slicePF,        //  8, 001000: SLICE 4
  endSustPF,      //  9, 001001: END SUSTAINS
  slicePF,        // 10, 001010: SLICE 5
  xPF,            // 11, 001011:
  slicePF,        // 12, 001100: SLICE 6
  xPF,            // 13, 001101:
  slicePF,        // 14, 001110: SLICE 7
  xPF,            // 15, 001111:
  cuePF,          // 16, 010000: CUE 0
  xPF,            // 17, 010001:
  cuePF,          // 18, 010010: CUE 1
  xPF,            // 19, 010011:
  cuePF,          // 20, 010100: CUE 2
  xPF,            // 21, 010101:
  cuePF,          // 22, 010110: CUE 3
  xPF,            // 23, 010111:
  cuePF,          // 24, 011000: CUE 4
  xPF,            // 25, 011001:
  cuePF,          // 26, 011010: CUE 5
  xPF,            // 27, 011011:
  cuePF,          // 28, 011100: CUE 6
  xPF,            // 29, 011101:
  cuePF,          // 30, 011110: CUE 7
  xPF,            // 31, 011111:
  offPF,          // 32, 100000: OFF
  rangePF,        // 33, 100001: RANGE
  cueOffPF,       // 34, 100010: CUE OFF 1
  xPF,            // 35, 100011:
  cueOffPF,       // 36, 100100: CUE OFF 2
  xPF,            // 37, 100101:
  cueOffPF,       // 38, 100110: CUE OFF 3
  xPF,            // 39, 100111:
  cueOffPF,       // 40, 101000: CUE OFF 4
  xPF,            // 41, 101001:
  cueOffPF,       // 42, 101010: CUE OFF 5
  xPF,            // 43, 101011: 
  cueOffPF,       // 44, 101100: CUE OFF 6
  xPF,            // 45, 101101:
  cueOffPF,       // 46, 101110: CUE OFF 7
  xPF,            // 47, 101111:
  cueOffPF,       // 48, 110000: CUE OFF 0
  loadPF,         // 49, 110001: LOAD SONG PAGE 0
  cueOffPF,       // 50, 110010: CUE OFF 1
  loadPF,         // 51, 110011: LOAD SONG PAGE 3
  cueOffPF,       // 52, 110100: CUE OFF 2
  loadPF,         // 53, 110101: LOAD SONG PAGE 2
  cueOffPF,       // 54, 110110: CUE OFF 3
  xPF,            // 55, 110111:
  cueOffPF,       // 56, 111000: CUE OFF 4
  loadPF,         // 57, 111001: LOAD SONG PAGE 1
  cueOffPF,       // 58, 111010: CUE OFF 5
  xPF,            // 59, 111011:
  cueOffPF,       // 60, 111100: CUE OFF 6
  xPF,            // 61, 111101:
  cueOffPF,       // 62, 111110: CUE OFF 7
  xPF             // 63, 111111: (TOGGLE EDIT MODE is handled on command-button-press, not grid-button-press)
};

// Associates the positional bits in BUTTONS with raw pitch-offset values.
// (Within a given block in this array: first value = top left; each rightward row of note-values represents a physical downward column of buttons)
const byte GRIDS[] PROGMEM = {
  
   0,  1,  2,  3,  4,  5, // Config 0: Guitar-like, with horizontally adjacent tritones
   6,  7,  8,  9, 10, 11,
  12, 13, 14, 15, 16, 17,
  18, 19, 20, 21, 22, 23,
  
   5,  4,  3,  2,  1,  0, // Config 1: Reverse-guitar-like, with horizontally adjacent tritones
  11, 10,  9,  8,  7,  6,
  17, 16, 15, 14, 13, 12,
  23, 22, 21, 20, 19, 18,
  
   0,  1,  2,  3,  4,  5, // Config 2: Guitar-like, with horizontally adjacent major fourths
   5,  6,  7,  8,  9, 10,
  10, 11, 12, 13, 14, 15,
  15, 16, 17, 18, 19, 20,
    
   5,  4,  3,  2,  1,  0, // Config 3: Reverse-guitar-like, with horizontally adjacent major fourths
  10,  9,  8,  7,  6,  5,
  15, 14, 13, 12, 11, 10,
  20, 19, 18, 17, 16, 15,
  
  20, 16, 12,  8,  4,  0, // Config 4: Ascending rows, bottom-left to top-right
  21, 17, 13,  9,  5,  1,
  22, 18, 14, 10,  6,  2,
  23, 19, 15, 11,  7,  3,
  
   0,  4,  8, 12, 16, 20, // Config 5: Descending rows, top-left to bottom-right
   1,  5,  9, 13, 17, 21,
   2,  6, 10, 14, 18, 22,
   3,  7, 11, 15, 19, 23
  
};
