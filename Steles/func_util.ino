

// Apply an offset to a given position in the current RECORDSEQ, applying either a negative or positive chirality to the OFFSET value
word applyOffset(char chiral, word p) {
  word len = seqLen(RECORDSEQ); // Get the RECORDSEQ's length, in ticks
  // Apply the offset to the given position (with positive or negative chirality), and wrap its new value (even if negative)
  return word((int(p) + (OFFSET * chiral)) + len) % len;
}

// Apply QUANTIZE and QRESET to a given point in the current RECORDSEQ
word applyQuantize(word p) {
  
  byte down = (POS[RECORDSEQ] % (QRESET ? QRESET : 65535)) % QUANTIZE; // Get the distance from the previous QUANTIZE-point
  byte up = min(QRESET, QUANTIZE) - down; // Get distance to next QUANTIZE-point, compensating for QRESET
  
  return (p + ((down <= up) ? (-down) : up)) % seqLen(RECORDSEQ); // Return the QUANTIZE-and-QRESET-adjusted insertion-point
  
}

// Get a change-value from a given keystroke, and apply that change to the given "v" value within the given "low"-to-"high" range
byte fullChange(byte col, byte row, byte v, byte low, byte high) {
  
  int vbig; // Will hold the modified "v" value, before it is brought back into byte-size format
  
  if (row) { // If the given keystroke was in one of the 5 bottom button-rows...
    
    // Add or subtract a change-value to "vbig" that corresponds to the given row.
    // (Note: The change-value will be one of the following: 16, 8, 4, 2, 1, -1, -2, -4, -8, -16.)
    vbig = int(v) + (char(32 >> row) * (char(col & 2) - 1));
    
  } else { // Else, if this is the top row...
    
    // Get the most-significant-bit of the given "high" value (this will be used to calculate the bitshift limit)
    byte msb = high | (high >> 1);
    msb |= msb >> 2;
    msb |= msb >> 4;
    msb &= ~(msb >> 1);
    
    // Bitshift "v" either left or right by 1, depending on the given button-column.
    // Also: If its highest bit is filled, do nothing; this simulates an intuitive "limit" instead of maxing out empty bits.
    vbig = (col & 2) ? (v >> 1) : (v << !(v & msb));
    
  }
  
  if (high) { // If "high" isn't 0, then the value that's being changed should be clamped instead of wrapped. So...
    // Convert "vbig" to a byte, clamp it between the given "low" and "high" limits, and return it.
    return (vbig < low) ? low : ((vbig > high) ? high : byte(vbig));
  }
  
  return byte(vbig); // If "high" was set to 0, then wrap the value instead of clamping it, and return it.
  
}

// Check whether the current point in RECORDSEQ is an insertion-point, when adjusted for QUANTIZE, QRESET, and OFFSET
byte isInsertionPoint() {
  return !((applyOffset(-1, POS[RECORDSEQ]) % (QRESET ? QRESET : 65535)) % QUANTIZE);
}

// Get a note that corresponds to a raw keypress, and also apply BASENOTE and OCTAVE via modPitch()
byte modKeyPitch(byte col, byte row) {
  // Return a note-value based on the keypress within the current GRIDCONFIG
  return modPitch(pgm_read_byte_near(GRIDS + (GRIDCONFIG * 24) + (col * 6) + row));
}

// Modify a given pitch by applying an OCTAVE value, and fix it if it goes out of range
byte modPitch(byte pitch) {
  pitch += BASENOTE + (OCTAVE * 12); // Add the BASENOTE and OCTAVE to the given raw pitch
  while (pitch > 127) { pitch -= 12; } // While the pitch is above the MIDI-NOTE range, reduce it by octaves until it's within range
  return pitch; // Return the modified pitch-value
}

// Apply the HUMANIZE-modifier to a given velocity-value, and keep it within the proper bounds
byte modVelo(byte v) {
  // Subtract a random humanize-offset from the given velocity, being careful not to underflow; then return it
  return v - (RANDS[0] % (min(v, HUMANIZE) + 1));
}

// Get a given seq's length, in ticks
word seqLen(byte seq) {
  return word((STATS[seq] & 63) + 1) * TIMESIG3;
}

// Shift the GLOBAL CUE by the given amount, wrapping it around its TIMESIG-based limits
void shiftCuePoint(int change) {
  word ws = word(RANGE) * TIMESIG3; // Get the wrap-size of the current RANGE, for wrapping the CUE-point correctly
  CURTICK = word(((long(CURTICK) + change) % ws) + ws) % ws; // Shift the global cue-point, wrapping in either direction
}

// Shift every playing-seq's position by the given amount, wrapping them around their SIZE-based limits
void shiftPlayingSeqs(int change) {
  for (byte seq = 0; seq < 48; seq++) { // For each seq...
    if (STATS[seq] & 128) { // If the seq is playing...
      word size = seqLen(seq); // Get the seq's size, in ticks
      POS[seq] = word(((long(POS[seq]) + change) % size) + size) % size; // Shift the seq's position, wrapping in either direction
    }
  }
}

// Update TIMESIG3, then wrap the CURTICK around the number of total measures, to keep it from going out-of-bounds
void tsUpdateWrap() {
  TIMESIG3 = TIMESIG1 * TIMESIG2; // Get the total number of ticks in a measure
  CURTICK %= word(RANGE) * TIMESIG3; // Wrap the global-current-tick around the new RANGE-of-measures, in case the latter is shorter
}

// Set the RANDS array to contain semirandom numbers, using an 8-bit xorshift algorithm
void xorShift() {
  memmove(RANDS + 1, RANDS, 4);
  RANDS[4] ^= (RANDS[4] << 3);
  RANDS[0] ^= (RANDS[0] >> 5) ^ RANDS[4] ^ (RANDS[4] >> 2);
}


