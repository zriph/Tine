

// End the sustain in a given SUST-slot
void endSustain(byte n) {
  
  SUST[n + 2] = 127; // Set the third SUST-byte to 127, so various synths don't get confused when parsing it as part of a NOTE-OFF
  Serial.write(SUST + n, 3); // Send that NOTE-OFF
  
  // Remove the now-obsolete SUST-entry for the note that just got turned off:
  if (n < 27) { // If this isn't the array's bottom note-slot...
    memmove(SUST + n, SUST + n + 3, 27 - n); // Move all lower notes one slot upwards
  }
  SUST_COUNT -= 3; // Reduce the array's entry-counting var
  memset(SUST + SUST_COUNT, 0, 3); // Empty out the bottommost note-slot's former position
  
}

// If the bottommost SUSTAIN is filled, send its NOTE-OFF prematurely
void clipBottomSustain() {
  if (SUST_COUNT < 30) { return; } // If the SUSTAIN array isn't full, exit the function
  endSustain(27); // Send a premature NOTE-OFF for the oldest active sustain
}

// Process one tick worth of duration for all notes in the SUSTAIN system
void processSustains() {

  byte n = 0; // Value to iterate through each sustain-position
  
  while (n < SUST_COUNT) { // For every sustain-entry in the SUST array (if there are any)...
  
    byte n2 = n + 2; // Get the key of the sustain's DURATION byte
    
    if (SUST[n2]) { // If any duration remains...
      SUST[n2]--; // Reduce the duration by 1
      n += 3; // Move on to the next sustain-position normally
    } else { // Else, if the remaining duration is 0...
      endSustain(n); // End the sustain in SUST-slot "n"
      // "n" doesn't need to be increased here, since the next sustain occupies the same index now
    }
    
  }
  
}

