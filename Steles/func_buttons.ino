

// If any custom PLAY MODE commands are currently or were previously held, flag the GUI to be refreshed
void refreshPlayCustoms(byte ctrl, byte oldcmds) {
  
  if (
    (ctrl == BINARY_RANGE) // If the current command is a RANGE command...
    || (oldcmds == BINARY_RANGE) // Or the previous command was a RANGE command...
    || (ctrl == BINARY_SHIFTPOS) // Or the current command is a SHIFT POSITION command...
    || (oldcmds == BINARY_SHIFTPOS) // Or the previous command was a SHIFT POSITION command...
    || (ctrl == BINARY_LOAD0) // Or the current command is a LOAD FILE (loadpage 0) command...
    || (oldcmds == BINARY_LOAD0) // Or the previous command was a LOAD FILE (loadpage 0) command...
    || (ctrl == BINARY_LOAD1) // Or the current command is a LOAD FILE (loadpage 1) command...
    || (oldcmds == BINARY_LOAD1) // Or the previous command was a LOAD FILE (loadpage 1) command...
    || (ctrl == BINARY_LOAD2) // Or the current command is a LOAD FILE (loadpage 2) command...
    || (oldcmds == BINARY_LOAD2) // Or the previous command was a LOAD FILE (loadpage 2) command...
    || (ctrl == BINARY_LOAD3) // Or the current command is a LOAD FILE (loadpage 3) command...
    || (oldcmds == BINARY_LOAD3) // Or the previous command was a LOAD FILE (loadpage 3) command...
  ) {
    TO_UPDATE |= 249; // Flag the top, and bottom 5, LED-rows for an update
  }
  
}

// Interpret an incoming keystroke, using a given button's row and column
void assignKey(byte col, byte row, byte oldcmds) {
  
  if (LOADHOLD) { // If LOADHOLD is active from a previous event...
    LOADHOLD = 0; // Turn LOADHOLD off immediately
    TO_UPDATE |= 248; // Flag the bottom 5 LED-rows to update
  }
  
  byte ctrl = BUTTONS & B00111111; // Get which ctrl-buttons are being held
  
  if (col == 0) { // If the keystroke is in the leftmost column...
    
    arpClear(); // Clear the arpeggiation-system's variables
    
    if (ctrl == BINARY_MODETOGGLE) { // If this was the final keystroke in a TOGGLE EDIT MODE command...
      
      toggleEditMode(); // Toggle into, or out of, EDIT MODE
      
    } else if (EDITMODE) { // Else, if TOGGLE EDIT MODE wasn't pressed, and EDIT MODE is active...
      
      concludeHeldNote(0, 1); // Conclude held note, not recording it, but sending a NOTE-OFF
      
      TO_UPDATE |= 251; // Flag the top 2 LED-rows, and bottom 6 LED-rows, for updating
      
    } else { // Else, if this is PLAY MODE...
      
      refreshPlayCustoms(ctrl, oldcmds); // If any custom PLAY MODE commands were involved, flag their GUI to be refreshed
      
    }
    
  } else { // Else, if the keystroke is in any of the other columns...
    
    if (EDITMODE) { // If EDIT MODE is active...
      ((CmdFunc) pgm_read_word_near(&COMMANDS[ctrl])) (col - 1, row); // Run a function that corresponds to the keypress
    } else { // Else, if PLAY MODE is active...
      ((PlayFunc) pgm_read_word_near(&PLAYCMDS[ctrl])) (ctrl, col - 1, row); // Run a function that corresponds to the keypress
    }
    
  }
  
}

// Interpret a key-release according to whatever command-mode is active
void unassignKey(byte col, byte row, byte oldcmds) {
  
  if (EDITMODE) { // If EDIT MODE is active...
    
    if (col == 0) { // If this button-release was in the leftmost column...
      
      if (oldcmds == BINARY_MOVEBYTICK) { // If MOVE BY TICK was previously being held...
        while (SUST_COUNT) { // While there are any active SUSTAINs...
          endSustain(0); // End the topmost queued SUSTAIN
        }
      }
      
      concludeHeldNote(1, 1); // Conclude held note, recording it and sending a NOTE-OFF
      
      TO_UPDATE = 255; // Flag all LED-rows for updating
      
    } else { // Else, if this button-release was on a regular note-button...
      
      arpRelease(); // Check whether this was the last released arp-note, and clear the arpeggiation-system if so
      
      if (
        (HELDBUTTON & 128) // If there is currently a held-command...
        && ((BUFWRITE[0] & 240) == 144) // And the current held-command is a NOTE-ON...
        && ((HELDBUTTON & 127) == (((col - 1) * 6) + row)) // And the button that was released is the same as the held-note's button...
      ) {
        concludeHeldNote(1, 1); // Conclude held note, recording it and sending a NOTE-OFF
      }
      
      TO_UPDATE |= 248; // Flag the bottom 5 LED-rows for updating
      
    }
    
  } else { // Else, if this is PLAY MODE...
    
    if (oldcmds == BINARY_SWAP) { // If a SWAP keychord was previously held...
      SWAPLAST = 255; // Set SWAPLAST to "no seq has been pressed during the current SWAP command"
    } else { // Otherwise...
      // If any custom PLAY MODE commands were involved, flag their GUI to be refreshed
      refreshPlayCustoms(BUTTONS & B00111111, oldcmds);
    }
    
  }
  
}
