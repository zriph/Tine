
# Tine
Files for the "ATMEGA328P-PU"-based Tine project (in development).

# Fuse-byte settings
* efuse: B11111100 0xFC (brownout protection: <= 4 volt threshold)
* lfuse: B11111111 0xFF (do not divide clock by 8; no data-out line; maximum delay on startup; use 16MHz crystal oscillator circuit)
* hfuse: B11011001 0xD9 (defaults)

# How to prepare an ATMEGA328P chip for use in Tine
1. Use any of the standard methods to upload a bootloader onto the ATMEGA328P chip; or, socket the chip into Tine and use an AVR-ISP cable to upload the bootloader into the chip.
2. If you are using certain older types of Arduino board, or similar, to perform step (1), then you may have to connect a 10uF capacitor between the programmer-board's "RESET" and "GND" pins, before the bootloader-upload process will work. (Capacitor's positive side connects to RESET; negative side goes to GND. Importantly, note that this is on the programmer-board, not the target chip!)
3. While your bootloader-programming circuit is still connected, set the ATMEGA-chip's fuse-bits with a command of the type "avrdude -c [programmer type] -p m328p -P [USB port name] -b 19200 -U efuse:w:0xfc:m -U lfuse:w:0xff:m -U hfuse:w:0xd9:m"
4. The ATMEGA328P chip should now be running at the correct clock-rate and brownout-threshold.
5. Socket the ATMEGA328P chip into its Tine unit if you haven't already, and then refer to chapter 3 in Tine's manual to see how to upload firmware to Tine.


